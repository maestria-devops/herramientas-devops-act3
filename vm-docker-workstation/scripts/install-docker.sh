# Add Docker Repository
wget -qO - https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o $KEYRINGS_PATH/docker-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/docker-keyring.gpg] \
    https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
    | sudo tee /etc/apt/sources.list.d/docker.list

# Update Repositories
sudo apt-get update

# Install Docker
sudo apt-get install -y docker-ce docker-ce-cli containerd.io 
sudo usermod -aG docker vagrant # For no problems with packer to build images
